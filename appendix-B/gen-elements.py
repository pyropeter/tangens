from csv import Sniffer,DictReader
from sys import argv
from re import sub

def getParent(row):
    format = list(map(row.get, ["M Len", "M Decis", "UC Len", "UC Type"]))
    format = '  '.join(f"{x:>3s}" for x in format)

    return {
        ' 12    2   12  num': "Decimal2",
        ' 12    3   12  num': "Decimal3",
        ' 12        12     ': "Element",
        ' 11        12     ': "Element1112",
        ' 12        36     ': "Element1236",
        ' 12               ': "ElementM",
        '  8               ': "ElementM8",
        '           12     ': "ElementUC",
        ' 12        12  num': "Numeric",
        '  5         5  num': "Numeric5",
    }[format]

def getName(row):
    num = int(row["DE#"])
    name = sub("[^\w\d]+", "", row["Displaytext"])

    return f"DE_{name}_{num}"

f = open(argv[1], newline='')
dialect = Sniffer().sniff(f.read(1024))
f.seek(0)
reader = DictReader(f, dialect=dialect)
rows = list(reader)

rows.sort(key=lambda r: int(r["DE#"]))

des = {}

print("#")
print("# DO NOT EDIT THIS FILE")
print("# This file was generated by appendix-B/gen-elements.py")
print("# DO NOT EDIT THIS FILE")
print("#")
print()
print("from .elements_base import *")
print()

for row in rows:
    num = int(row["DE#"])
    parent = getParent(row)
    name = getName(row)
    dt = row["Displaytext"]

    print(f"class {name}({parent}):")
    print(f"    _displayText = '{dt}'")
    print()

    des[num] = name

print("des = {")
for num, name in des.items():
    print(f"    {num:2d}: {name},")
print("}")

