This folder contains the tables from appendix B of [HHD 1.4] in
machine-readable form.

Also contained are scripts to translate the tables to python modules.


#### elements.csv

This file contains all information from B.1 as well as table 3 and 6.
The columns contain the following:

`DE#` is the data element number. It has two digits for the elements
from B.1 (Datenelemente) and one digit for the elements from table
3 and 6 (Basisdatenelemente). This corresponds with the format used
to reference elements in the Start Code.

`M Len` is the maximum length of the element data when using the manual
data entry method. This is also the length of the padded element data
field in VisData in bytes. All element data needs to be at least one
character long when using the manual data entry method. (see 4.6.2)

`M Decis` is the number of decimals to be used in the element data when
using the manual data entry method.
If specified, the element data must end with a comma character and the
given number of digits. The comma must be prefixed by an integral part
of at least one digit. Besides the one comma and the digits, no other
characters are allowed inside the element data.
If `M Decis` is empty, only digits are allowed inside the element data.

`UC Len` is the maximum length of the element data when using the
unidirectional coupling data entry method.
In contrast with `M Len`, this information is only used for input
validation, as no padding takes place with the UC method.

`UC Type` specifies the accepted character set when using the
unidirectional coupling data entry method.
A value of `num` specifies the character set `[0-9,.]`. ("numerisch")
An empty value specifies the full ZKA character set. ("Freitext")

`Displaytext` specifies the text used to display a data element.
This text prefixes the element data when generating the VisData string.

