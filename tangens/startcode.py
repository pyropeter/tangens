from re import compile, match, ASCII, VERBOSE

from .elements import *
from .util import toBCD

"""
Sources

[HHD 1.4] Schnittstellenspezifikation für die ZKA-Chipkarte -
        HandHeldDevice (HHD) zur TAN-Erzeugung, Version 1.4 Final
        Version, 07.05.2010, Zentraler Kreditausschuss
"""

"""
The following regular expression implements the common part of the
Start-Code checks mandated in 4.6.1 and 4.7.1 [HHD 1.4].
Please note that both sections describe mostly the same checks, but
are written in different style to make this hard to notice.
"""
_startCodeCommon = compile(r"""
    0 ( [128] \d{,6} | 9 ) $
    |
    1   \d    \d     \d{,9} $       # 1 v₁ v₂ z…z
    |
    2   \d    \d ( 0 \d{,8} |       # 2 v₁ v₂ 0 z…z
        [1-9] \d ( 0 \d{,6} |       # 2 v₁ v₂ p₁ p₂ 0 z…z
        [1-9] \d ( 0 \d{,4} |       # 2 v₁ v₂ p₁ p₂ s₁ s₂ 0 z…z
        [1-9] \d     \d{,3} ))) $   # 2 v₁ v₂ p₁ p₂ s₁ s₂ t₁ t₂ z…z
    |
    7 [1234567890] \d{,6} $
    |
    8 [0-8] [0-8] \d{,5} $
    |
    9 [1-9] [0-8] [0-8] \d{,4} $
""", ASCII | VERBOSE)

def _genVisDataTerminator(qty):
    "Terminator for VisData according to 4.9 [HDD 1.4]"

    if qty <= 14:
        return bytes([0xB0 + qty])
    else:
        return bytes([0xB0, qty])

def genVisData(elements):
    r"""
    >>> genVisData([DE_IBAN_8("12345", True), DE_Betrag_1("1,23", True)])
    b'\xe1IBAN\xe112345\xe1Betrag\xe11,23\xb4'
    >>> genVisData([DE_IBAN_8("1234567890")])
    b'\xe1IBAN\xe1\xff\xff1234567890\xb2'
    """

    viaUC = [x.viaUC for x in elements]
    if (not all(viaUC)) and any(viaUC):
        raise ValueError()

    blocks = []

    for element in elements:
        blocks += element.toVisData()

    return b''.join(blocks) + _genVisDataTerminator(len(blocks))

class StartCode(Element):
    _displayText = "Start-Code:"

    def _viaM(self):
        """
        Checks if Start-Code is valid for use with manual data entry
        (according to 4.6.1 [HHD 1.4])
        """

        if self.value == "":
            return True

        if not _startCodeCommon.match(self.value):
            return False

        return True

    def _viaUC(self):
        """
        Checks if Start-Code is valid for use with the unidirectional
        interface (according to 4.7.1 [HHD 1.4])
        """

        if not _startCodeCommon.match(self.value):
            return False

        if match("83|8.3|9.3|9..3|01", self.value):
            return False

        return True

    def toVisData(self):
        r"""
        >>> StartCode("88112", True).toVisData()
        [b'\xe1Start-Code:', b'\xe0\x88\x11/']
        >>> StartCode("88112").toVisData()
        [b'\xe1Start-Code:', b'\xe0\xff\xf8\x81\x12']
        """

        length = 4 if self.value[0] in "0789" else 6

        text = self._displayText.encode("zka")
        value = toBCD(self.value, 0 if self.viaUC else length)

        return [b"\xE1" + text, b"\xE0" + value]

def _elementsFromB(code):
    code = code.replace("0", "")

    return [des[int(c)] for c in code]

def elementsFromStartCode(code):
    """
    >>> elementsFromStartCode("88112345")
    [<class 'tangens.elements.DE_IBAN_8'>, <class 'tangens.elements.DE_Betrag_1'>]
    """

    if code.startswith("8"):
        return _elementsFromB(code[1:3])

    raise NotImplementedError()

if __name__ == "__main__":
    import doctest
    doctest.testmod()

