from re import match

def pairs(l):
    """
    Group an iterable of even length into pairs

    >>> pairs(range(10))
    [(0, 1), (2, 3), (4, 5), (6, 7), (8, 9)]
    """

    l = list(l)

    if len(l) % 2 == 1:
        raise ValueError()

    return list(zip(l, l[1:]))[::2]

def toBytes(s):
    """
    >>> toBytes('12 34 5F') == bytes([0x12, 0x34, 0x5F])
    True
    """

    s = s.replace(" ", "")
    return bytes([int(a+b, base=16) for (a,b) in pairs(s)])

def toHexString(s):
    """
    >>> toHexString(bytes([0x12, 0x34, 0x5F]))
    '12 34 5F'
    """

    return ' '.join([f"{c:02X}" for c in s])

def toBCD(num, padLen=0):
    """
    >>> toHexString(toBCD("12345"))
    '12 34 5F'
    >>> toHexString(toBCD("12345", 4))
    'FF F1 23 45'
    >>> toHexString(toBCD(""))
    ''
    """

    num = str(num)

    if not match("[0-9]*$", num):
        raise ValueError()

    if padLen and len(num) > 2*padLen:
        raise ValueError()

    while len(num) < 2*padLen:
        num = "F" + num

    if len(num) % 2 == 1:
        num = num + "F"

    return toBytes(num)

if __name__ == "__main__":
    import doctest
    doctest.testmod()

