from re import match

from . import charsets

class Element(object):
    _maxLenM  = 12
    _maxLenUC = 12

    def __init__(self, value, viaUC=False):
        self.value = value
        self.viaUC = viaUC

        # input normalization and validation
        if viaUC:
            self._viaUC()
        else:
            self._viaM()

        try:
            self.value.encode("zka")
        except UnicodeEncodeError:
            raise ValueError()

        if match("[_ ]|.*[_ ]", self.value):
            raise ValueError()

    def _viaUC(self):
        if not 1 < len(self.value) <= self._maxLenUC:
            raise ValueError()

    def _viaM(self):
        if not 1 <= len(self.value) <= self._maxLenM:
            raise ValueError()

        if not match("[0-9]+$", self.value):
            raise ValueError()

    def __repr__(self):
        """
        >>> Element("12345")
        Element('12345', False)
        """

        return f"{type(self).__name__}({self.value!r}, {self.viaUC!r})"

    def toVisData(self):
        text = self._displayText.encode("zka")
        value = self.value.encode("zka")

        if not self.viaUC:
            value = value.rjust(self._maxLenM, b'\xff')

        return [b"\xE1" + text, b"\xE1" + value]

class Element1112(Element):
    _maxLenM = 11

class Element1236(Element):
    _maxLenUC = 36

class ElementM(Element):
    def _viaUC(self):
        raise ValueError()

class ElementM8(ElementM):
    _maxLenM = 8

class ElementUC(Element):
    def _viaM(self):
        raise ValueError()

class Numeric(Element):
    def _viaUC(self):
        super()._viaUC()

        if not match("[0-9,.]+$", self.value):
            raise ValueError()

class Numeric5(Numeric):
    _maxLenM  = 5
    _maxLenUC = 5

class Decimal(Numeric):
    def _viaM(self):
        self.value.replace(".", "")

        m = match("0*([1-9][0-9]*)?(?:,([0-9]*))?$", self.value)
        if not m or len(m.group(2) or "") > self._decimals:
            raise ValueError()

        fractional = m.group(2) or ""
        while len(fractional) < self._decimals:
            fractional += "0"

        self.value = f"{m.group(1) or '0'},{fractional}"

        if not 1 <= len(self.value) <= self._maxLenM:
            raise ValueError()

class Decimal2(Decimal):
    _decimals = 2

class Decimal3(Decimal):
    _decimals = 3

if __name__ == "__main__":
    import doctest
    doctest.testmod()

