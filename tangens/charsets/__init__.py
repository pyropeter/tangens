import codecs

from .zka import getregentry as zka

def search_function(encoding):
    if encoding == "zka":
        return zka()

    return None

codecs.register(search_function)

